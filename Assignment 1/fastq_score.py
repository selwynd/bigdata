from multiprocessing import Process, Queue
import os

'''
Author  :   Selwyn Dijkstra
Date    :   16-05-2018
Title   :   Assignment 1

To run use the command "python3 fastq_score.py (Your file) -n (Amount of processes)"
'''

def get_score(file,byterange,q):
    '''This process will start at a specific byte and continue reading untill
    a @ has been found. This is the beginning of a read. Once the start location has been
    found the average score will be determined '''

    offset = None
    results = []
    with open(file) as read:
        read.seek(byterange)
        t = read.readlines()

        #Reads the first lines of the file to check for a start of a read
        #Known bug is that a string with a start is sliced and thus not findable in the first 4 lines
        #Therefore the first 6 lines are read just incase 1 read gets spliced and thus not findable as start
        for i, line in enumerate(t[0:6]):
            if line.startswith('@cluster'):
                if offset == None:
                    offset = i
                elif isinstance(offset,int):
                    pass

        #When the beginning of a read is defined the calculation can begin
        #Using the offset the 2nd and 4th line are compared for length
        if isinstance(offset,int):
            total = 0
            for i,line in enumerate(t):
                if i-offset < 0:
                    pass
                if (i-offset + 1) % 2 == 0:
                    check_length = len(line)
                if (i-offset + 1) % 4 == 0:
                    if len(line) == check_length:
                        for character in line:
                            total += int((ord(character) - 33))
                        average = total / len(line)
                        results.append(("Average per base:",int(average),"Total of read :",total))
                        average= 0
                        total = 0
        q.put(results)


def create_jobs(fastq, cores):
    '''The size of the file will be split by the amount of processes.
    Each Process with have a different chunch of the file and calculate
    the average score per base'''

    proclist = []
    q = Queue()
    t = os.stat(fastq.name)
    chunck = int((t.st_size)/cores)
    chunck_counter = 0

    #Processes will be made for the amount of cores selected and added to the proclist
    for core in range(0,int(cores)):
        p = Process(target=get_score, args=((fastq.name),chunck_counter,q))
        p.start()
        proclist.append(p)
        chunck_counter += chunck

    print("Amount of processes running | {} \t| \nAmount of bytes per process | {} \t|".format(cores, chunck))
    #Prints the results off all processes and terminates these processes
    for x in proclist:
        x.join()
        print(len(q.get()))


def main(argv):
    if argv is None:
        argv = sys.argv

    try:
        if argv[2] == '-n':
            cores = argv[3]
            create_jobs(open(argv[1]), int(cores))
    except IndexError as err:
        print('No amount of processes selected. Default value of 4 is used')
        if err:
            create_jobs(open(argv[1]), cores=4)


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
